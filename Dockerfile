# Build
FROM node:latest AS builder

RUN git clone https://github.com/bootstrap-vue/bootstrap-vue.git /bootstrap-vue

WORKDIR /bootstrap-vue

RUN yarn install
RUN yarn docs-gen

# Serve
FROM nginx:alpine

COPY --from=builder /bootstrap-vue/docs-dist /usr/share/nginx/html
